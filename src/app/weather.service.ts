import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  apiKey = 'b14ac9d901ce813af97fe13addba514a';
  url;

  constructor(private http: HttpClient) {
    this.url = 'http://api.openweathermap.org/data/2.5/forecast?q=';
}

getWeather(city, code) {
    return this.http.get(this.url + city + ',' + code + '&AppId=' + this.apiKey )
}
}
