
# Stage 1
FROM node:alpine AS node
WORKDIR /app
COPY . .

RUN npm install && \
    npm run build

FROM nginx:alpine
COPY --from=node /app/dist/WApp /usr/share/nginx/html/
